App.comment = App.cable.subscriptions.create "CommentChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # # Called when there's incoming data on the websocket for this channel
    unless data.content.blank?
      $('.comment_form input[type=text]').val("");
      $('#comment_list_post_' + data.postId).append '<span class="comment_detail"> 
              <strong> <a href="./'+ data.userUrl + '">' + data.name + '</a> </strong>
              ' + data.content + ' </span>'
    
    
