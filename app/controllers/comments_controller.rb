class CommentsController < ApplicationController
	def index
    	@comments = @post.comments.includes(:user)
  	end
	def create
		#@comment = @post.comments.create(comment_params)
		@post = Post.find_by_id(params[:post_id])
		puts comment_params
		@comment = @post.comments.create(comment_params)
		
		if @comment.save
			ActionCable.server.broadcast 'comment_channel',
				content: @comment.content,
				postId: @post.id,
				userUrl:  user_path(@comment.user),
				name: @comment.user.name
			# respond_to :js
			head :ok
		else
			flash.now[:danger] = "error"
		end
	end
	private
    def comment_params
    	params.required(:comment).permit :user_id, :post_id, :content
  	end
end
